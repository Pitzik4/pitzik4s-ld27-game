package net.pitzik4.games.ld48.ld27 {
	import org.flixel.*;
 
	public class HelpState extends FlxState {
		[Embed(source = "/gfx/help.png")] public static const ImgHelp:Class;
		
		public var help:FlxSprite;
		
		override public function create():void {
			help = new FlxSprite();
			help.loadGraphic(ImgHelp, true, false, 160, 120);
			help.frame = 0;
			add(help);
		}
		override public function update():void {
			super.update();
			if(FlxG.keys.justReleased("Z")) {
				if(help.frame < 6) {
					help.frame++;
				} else {
					FlxG.switchState(new TitleState());
				}
			}
		}
	}
}
