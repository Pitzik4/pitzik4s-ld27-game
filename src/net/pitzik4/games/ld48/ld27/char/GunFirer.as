package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class GunFirer extends Character {
		[Embed(source = "/snd/shot.mp3")] public static const SndShot:Class;
		
		public var shotsound:FlxSound;
		public var shoottime:Number;
		public var toShoot:Character;
		
		public function GunFirer(ps:PlayState, char:uint, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, char, x, y, id);
			shotsound = FlxG.loadSound(SndShot);
			addAnimation("shoot", addChar([13, 15, 15, 15, 17, 17, 15, 13, 0], char), 15, false);
			_extsprite.addAnimation("shoot", addChar([13, 15, 15, 15, 17, 17, 15, 13, 30], char+1), 15, false);
		}
		public function ifNotHuman(interacted:Interactable):void {
			super.interactWith(interacted);
		}
		public function interactTextObject(interacted:Interactable):String {
			return null;
		}
		override public function interactTextOverride(interacted:Interactable):String {
			return interacted is Character ? "Kill " + interacted.getCharName() : interactTextObject(interacted);
		}
		override public function interactWith(interacted:Interactable):void {
			if(interacted == this) {
				super.interactWith(interacted);
			} else if(interacted is Character) {
				play("shoot");
				_extsprite.play("shoot");
				shotsound.play();
				interacttime = 1;
				var ipos:FlxPoint = interacted.getpos();
				if(ipos.x < x) {
					facing = LEFT;
				} else {
					facing = RIGHT;
				}
				shoottime = 0.5;
				toShoot = interacted as Character;
			} else {
				ifNotHuman(interacted);
			}
		}
		override public function shouldWalk(interacted:Interactable):Boolean {
			return interacted == null || !(interacted is Character);
		}
		override public function update():void {
			super.update();
			shoottime -= FlxG.elapsed;
			if(shoottime <= 0) {
				shoottime = 0;
				if(toShoot != null) {
					toShoot.play("death");
					toShoot.capable = false;
					toShoot = null;
				}
			}
		}
	}
}
