package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class Goon extends GunFirer {
		public var hostage:Boolean = false;
		
		public function Goon(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_GOON, x, y, id);
			addAnimation("death", addChar([16, 17, 17, 17, 17, 17, 18, 20], CHAR_GOON + 3), 10, false);
			addAnimation("hwalk", addChar([25, 26], CHAR_GOON), 3);
			addAnimation("hidle", addChar([25], CHAR_GOON));
			_extsprite.addAnimation("death", addChar([31, 31, 31, 31, 31, 31, 22, 24], CHAR_GOON), 10, false);
		}
		override public function getCharName():String {
			return hostage?"Goon and Kate":"Goon";
		}
		override public function play(anim:String, force:Boolean = false):void {
			super.play(hostage?"h"+anim:anim, force);
			if(anim == "death") {
				_extsprite.play(anim, force);
			} else {
				_extsprite.play("empty");
			}
		}
		override public function interactTextOverride(interacted:Interactable):String {
			return interacted is Kate ? "Take Kate hostage" : super.interactTextOverride(interacted);
		}
		override public function interactWith(interacted:Interactable):void {
			if(interacted is Kate) {
				(interacted as Kate).play("gone");
				(interacted as Kate).capable = false;
				(ps.characters[6] as Tyler).play("surrender");
				(ps.characters[6] as Tyler).capable = false;
				hostage = true;
				play("idle");
				movespeed *= 0.75;
			} else if(interacted is Character) {
				if(!hostage) super.interactWith(interacted);
			} else {
				super.interactWith(interacted);
			}
		}
		override public function shouldWalk(interacted:Interactable):Boolean {
			return (interacted is Kate) || super.shouldWalk(interacted);
		}
	}
}
