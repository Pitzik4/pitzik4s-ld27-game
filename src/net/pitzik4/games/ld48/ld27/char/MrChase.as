package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class MrChase extends Character {
		public function MrChase(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_CHASE, x, y, id);
			addAnimation("fixtie", addChar([13, 14, 14, 15, 15, 15, 14, 14, 13, 0], CHAR_CHASE), 10, false);
			addAnimation("death", addChar([16, 17, 17, 17, 17, 17, 18, 20], CHAR_CHASE), 10, false);
			_extsprite.addAnimation("death", addChar([31, 31, 31, 31, 31, 31, 19, 21], CHAR_CHASE), 10, false);
		}
		override public function dosomething():void {
			if(capable) {
				play("fixtie");
				interacttime = 1;
			}
		}
		override public function interactText(interacter:Character):String {
			if(interacter == this) {
				return "Fix tie";
			}
			return super.interactText(interacter);
		}
		override public function getCharName():String {
			return "Mr. Chase";
		}
		override public function play(anim:String, force:Boolean = false):void {
			super.play(anim, force);
			if(anim == "death") {
				_extsprite.play(anim, force);
			} else {
				_extsprite.play("empty");
			}
		}
	}
}
