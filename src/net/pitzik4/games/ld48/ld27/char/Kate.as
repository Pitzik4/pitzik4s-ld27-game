package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class Kate extends Character {
		public function Kate(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_KATE, x, y, id);
			addAnimation("takeread", addChar([13, 14], CHAR_KATE), 4, false);
			addAnimation("reading", [14 + CHAR_KATE]);
			addAnimation("fiddle", [15 + CHAR_KATE]);
			addAnimation("gone", [31]);
		}
		override public function interact(interacter:Character):void {
			super.interact(interacter);
		}
		override public function interactText(interacter:Character):String {
			return super.interactText(interacter);
		}
		override public function getCharName():String {
			return "Kate";
		}
	}
}
