package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.levels.Level;
	import org.flixel.*;
	import net.pitzik4.games.ld48.ld27.*;
	
	public class Character extends FlxSprite implements Interactable {
		[Embed(source = "/gfx/char.png")] public static const ImgChar:Class;
		
		public static const CHAR_BOB:uint  = 0;
		public static const CHAR_JAKE:uint = 32;
		public static const CHAR_KATE:uint = 64;
		public static const CHAR_JEN:uint  = 96;
		public static const CHAR_MIKE:uint = 128;
		public static const CHAR_TYLER:uint = 160;
		public static const CHAR_CHASE:uint = 192;
		public static const CHAR_GOON:uint = 224;
		
		public static var nextid:uint = 0;
		
		private var _dest:FlxPoint;
		private var _mvector:FlxPoint;
		private var _moving:Boolean = false;
		private var _distance:Number;
		protected var _extsprite:FlxSprite;
		public var capable:Boolean = true;
		public var movespeed:Number;
		public var interacting:Interactable = null;
		public var interacttime:Number = 0;
		public var id:uint;
		public var lvl:Level;
		public var ps:PlayState;
		
		public static function addChar(arr:Array, char:uint):Array {
			for(var i:uint = 0; i < arr.length; i++) {
				arr[i] = (arr[i] as uint) + char;
			}
			return arr;
		}
		
		public function Character(ps:PlayState, char:uint=0, x:Number=0, y:Number=0, id:uint = 0xFFFFFFFF) {
			super(x - 4, y - 8);
			this.ps = ps;
			_extsprite = new FlxSprite(x + 8, y - 32);
			_extsprite.loadGraphic(ImgChar, true, true, 16, 32);
			_extsprite.addAnimation("empty", [31]);
			_extsprite.play("empty");
			_mvector = new FlxPoint();
			_dest = new FlxPoint();
			loadGraphic(ImgChar, true, true, 16, 32);
			addAnimation("idle", [char]);
			addAnimation("walk", addChar([1, 2, 3, 4, 5, 6, 7, 8], char), 15);
			addAnimation("talk", addChar([9, 10], char), 4);
			addAnimation("shrug", addChar([11, 12, 12, 12, 12, 11], char), 12, false);
			stopwalk();
			height = 8;
			width = 8;
			offset.y = 24;
			offset.x = 4;
			movespeed = 1;
			if(id == 0xFFFFFFFF) {
				this.id = nextid++;
			} else {
				this.id = id;
			}
		}
		public function get dest():FlxPoint {
			return _dest;
		}
		public function set dest(x:FlxPoint):void {
			walkTo(x);
		}
		public function getFootPoint():FlxPoint {
			var p:FlxPoint = getMidpoint();
			p.y += height / 2;
			return p;
		}
		public function stopwalk():void {
			if(capable) {
				play("idle");
				_extsprite.play("empty");
				_moving = false;
				immovable = true;
			}
		}
		public function walkTo(pos:FlxPoint):void {
			if(!capable) return;
			var fp:FlxPoint = getFootPoint();
			if(pos.x > fp.x) {
				this.facing = RIGHT;
			} else if(pos.x < fp.x) {
				this.facing = LEFT;
			}
			_mvector.x = pos.x - fp.x; _mvector.y = (pos.y - fp.y) * 2;
			var dist:Number = Math.sqrt(_mvector.x * _mvector.x + _mvector.y * _mvector.y);
			if(dist == 0) {
				_mvector.x = 1;
			} else {
				_mvector.x = (_mvector.x * movespeed) / dist;
				_mvector.y = (_mvector.y * movespeed / 2) / dist;
			}
			_moving = true;
			_dest.x = pos.x;
			_dest.y = pos.y;
			_distance = dist;
			play("walk");
			immovable = false;
		}
		
		override public function update():void {
			super.update();
			if(capable) {
				var oi:Number = interacttime;
				interacttime -= FlxG.elapsed;
				if(interacttime <= 0) {
					interacttime = 0;
					if(oi > 0) {
						play(_moving ? "walk" : "idle");
						_extsprite.play("empty");
					}
				}
				if(_moving) {
					_distance -= movespeed;
					if(_distance <= movespeed || isTouching(0xFFFFFFFF)) {
						if(!isTouching(0xFFFFFFFF)) {
							x = dest.x-4;
							y = dest.y - 8;
						}
						stopwalk();
						if(interacting != null) {
							interactWith(interacting);
						}
						interacting = null;
					} else {
						x += _mvector.x;
						y += _mvector.y;
					}
				}
			}
			y = FlxU.max(y, lvl==null?80:lvl.y+80);
			_extsprite.x = x + (facing == RIGHT ? 12 : -20);
			_extsprite.y = y - 24;
			_extsprite.facing = facing;
			_extsprite.preUpdate();
			_extsprite.update();
			_extsprite.postUpdate();
		}
		
		public function interact(interacter:Character):void {
			if(!capable) return;
			if(interacter.x < x) {
				facing = LEFT;
			} else {
				facing = RIGHT;
			}
		}
		public function getInteractAnim(interacter:Character):String {
			return capable ? "talk" : "idle";
		}
		public function getScreenBox():FlxRect {
			var sxy:FlxPoint = getMidpoint();
			_rect.x = sxy.x - 8; _rect.y = sxy.y - 28; _rect.width = 16; _rect.height = 32;
			return _rect;
		}
		public function getInteractTime(interacter:Character):Number {
			return capable ? 1 : 0;
		}
		public function canInteract(interacter:Character):Boolean {
			if(!capable) return false;
			var mymid:FlxPoint = getMidpoint();
			var hismid:FlxPoint = interacter.getMidpoint();
			return FlxU.max(FlxU.abs(mymid.x - hismid.x), FlxU.abs(mymid.y - hismid.y)) <= 20;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			var pos:FlxPoint = getFootPoint();
			if(interacter.x < x) {
				pos.x -= 16;
			} else {
				pos.x += 16;
			}
			return pos;
		}
		public function dosomething():void {
			if(capable) {
				play("shrug");
				interacttime = 0.5;
			}
		}
		public function interactText(interacter:Character):String {
			if(!capable) return "Walk to";
			if(interacter == this) {
				return "Shrug";
			}
			return "Talk to " + getCharName();
		}
		public function facedir(interacter:Character):uint {
			if(x < interacter.x) {
				return LEFT;
			}
			return RIGHT;
		}
		public function interactTextOverride(interacted:Interactable):String {
			return null;
		}
		public function interactWith(interacted:Interactable):void {
			if (interacting.canInteract(this)) {
				play(interacting.getInteractAnim(this));
				interacttime = interacting.getInteractTime(this);
				facing = interacting.facedir(this);
				interacting.interact(this);
			}
		}
		override public function draw():void {
			super.draw();
			_extsprite.draw();
		}
		public function getpos():FlxPoint {
			return getFootPoint();
		}
		public function shouldWalk(interacted:Interactable):Boolean {
			return true;
		}
		public function getCharName():String {
			return "Somebody";
		}
		public function goToLevel(lvl:Level):void {
			ps.moveCharacter(this, lvl);
		}
	}
}
