package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class Jake extends Character {
		public function Jake(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_JAKE, x, y, id);
			addAnimation("type", addChar([13, 14], CHAR_JAKE), 4);
		}
		override public function interact(interacter:Character):void {
			super.interact(interacter);
		}
		override public function interactText(interacter:Character):String {
			return super.interactText(interacter);
		}
		override public function getCharName():String {
			return "Jake";
		}
	}
}
