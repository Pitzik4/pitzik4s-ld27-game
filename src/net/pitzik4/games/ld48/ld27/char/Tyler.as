package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class Tyler extends GunFirer {
		public function Tyler(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_TYLER, x, y, id);
			addAnimation("death", addChar([16, 17, 17, 17, 17, 17, 18, 20], CHAR_TYLER+3), 10, false);
			_extsprite.addAnimation("death", addChar([31, 31, 31, 31, 31, 31, 22, 24], CHAR_TYLER), 10, false);
			addAnimation("surrender", addChar([25, 26], CHAR_TYLER), 10, false);
		}
		override public function getCharName():String {
			return "Tyler";
		}
		override public function play(anim:String, force:Boolean = false):void {
			super.play(anim, force);
			if(anim == "death") {
				_extsprite.play(anim, force);
			} else {
				_extsprite.play("empty");
			}
		}
	}
}
