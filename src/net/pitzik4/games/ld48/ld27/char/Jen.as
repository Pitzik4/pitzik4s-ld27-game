package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class Jen extends Character {
		public function Jen(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_JEN, x, y, id);
			addAnimation("death", addChar([16, 17, 17, 17, 17, 17, 18, 20], CHAR_JEN-3), 10, false);
			_extsprite.addAnimation("death", addChar([31, 31, 31, 31, 31, 31, 16, 18], CHAR_JEN), 10, false);
		}
		override public function interact(interacter:Character):void {
			super.interact(interacter);
		}
		override public function interactText(interacter:Character):String {
			return super.interactText(interacter);
		}
		override public function getCharName():String {
			return "Jen";
		}
		override public function play(anim:String, force:Boolean = false):void {
			super.play(anim, force);
			if(anim == "death") {
				_extsprite.play(anim, force);
			} else {
				_extsprite.play("empty");
			}
		}
	}
}
