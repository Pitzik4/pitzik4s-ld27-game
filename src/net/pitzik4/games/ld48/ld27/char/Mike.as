package net.pitzik4.games.ld48.ld27.char {
	import net.pitzik4.games.ld48.ld27.*;
	import org.flixel.*;
	
	public class Mike extends Character {
		public function Mike(ps:PlayState, x:Number, y:Number, id:uint = 0xFFFFFFFF) {
			super(ps, CHAR_MIKE, x, y, id);
			addAnimation("crowbar", addChar([13, 15], CHAR_MIKE), 4);
			_extsprite.addAnimation("crowbar", addChar([13, 15], CHAR_MIKE + 1), 4);
			addAnimation("fiddle", addChar([17, 18], CHAR_MIKE), 4);
			addAnimation("death", addChar([16, 17, 17, 17, 17, 17, 18, 20], CHAR_MIKE+3), 10, false);
			_extsprite.addAnimation("death", addChar([31, 31, 31, 31, 31, 31, 22, 24], CHAR_MIKE), 10, false);
		}
		override public function interact(interacter:Character):void {
			super.interact(interacter);
		}
		override public function interactText(interacter:Character):String {
			return super.interactText(interacter);
		}
		override public function getCharName():String {
			return "Mike";
		}
		override public function play(anim:String, force:Boolean = false):void {
			super.play(anim, force);
			if (anim == "crowbar" || anim == "death") {
				_extsprite.play(anim, force);
			} else {
				_extsprite.play("empty");
			}
		}
	}
}
