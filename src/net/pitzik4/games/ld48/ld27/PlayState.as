package net.pitzik4.games.ld48.ld27 {
	import flash.utils.Dictionary;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	import net.pitzik4.games.ld48.ld27.char.*;
 
	public class PlayState extends FlxState {
		[Embed(source = "/mus/kitten-of-a-wizard.mp3")] public static const MusGame:Class;
		[Embed(source = "/gfx/char.png")] public static const ImgChar:Class;
		[Embed(source = "/gfx/cursor.png")] public static const ImgCursor:Class;
		[Embed(source = "/gfx/mute.png")] public static const ImgMute:Class;
		[Embed(source = "/snd/next.mp3")] public static const SndNext:Class;
		
		private var _point:FlxPoint;
		private var _rect:FlxRect;
		
		public var player:Character;
		public var cursor:FlxSprite;
		public var iables:Array;
		public var chars:FlxGroup;
		public var nonchars:FlxGroup;
		public var everything:FlxGroup;
		public var interacttext:FlxText;
		public var mutebutton:FlxSprite;
		public var oldcharacters:Dictionary;
		public var characters:Dictionary;
		public var timer:Number = 0;
		public var nextchar:uint = 0;
		public var levels:Dictionary;
		public var timertext:FlxText;
		public var levelgroup:FlxGroup;
		public var front:FlxGroup;
		public var won:Boolean = false;
		public var elevators:Boolean = false;
		public var computers:Boolean = true;
		public var nextsnd:FlxSound;
		
		override public function create():void {
			FlxG.worldBounds.width = 1600;
			_point = new FlxPoint();
			_rect = new FlxRect();
			oldcharacters = new Dictionary();
			characters = new Dictionary();
			levels = new Dictionary();
			iables = new Array();
			front = new FlxGroup(2);
			FlxG.playMusic(MusGame);
			TitleState.titlemusic = false;
			FlxG.bgColor = 0xFF444444;
			everything = new FlxGroup();
			chars = new FlxGroup();
			nonchars = new FlxGroup();
			levelgroup = new FlxGroup();
			startNew();
			add(levelgroup);
			add(everything);
			interacttext = new FlxText(4, 104, 152, "Walk here");
			interacttext.color = 0xFFFFFFFF;
			var sf:FlxPoint = new FlxPoint(0, 0);
			interacttext.scrollFactor = sf;
			add(interacttext);
			add(front);
			timertext = new FlxText(140, 104, 16, "10");
			timertext.color = 0xFFFFFFFF;
			timertext.scrollFactor = sf;
			add(timertext);
			mutebutton = new FlxSprite(148, 4);
			mutebutton.loadGraphic(ImgMute, true);
			mutebutton.addAnimation("on", [0]);
			mutebutton.addAnimation("off", [1]);
			mutebutton.scrollFactor = sf;
			add(mutebutton);
			cursor = new FlxSprite();
			cursor.immovable = true;
			cursor.loadGraphic(ImgCursor, true);
			cursor.addAnimation("no", [0]);
			cursor.addAnimation("yes", [1]);
			cursor.play("yes");
			add(cursor);
			nextsnd = FlxG.loadSound(SndNext);
		}
		public function addLevel(lvl:Level):void {
			levelgroup.add(lvl);
			levels[lvl.getIntName()] = lvl;
			var ch:Array = lvl.getCharacters(this);
			for(var i:uint = 0; i < ch.length; i++) {
				addCharacter(ch[i] as Character, lvl);
			}
			ch = lvl.getInteractables(this);
			for (i = 0; i < ch.length; i++) {
				var c:Interactable = ch[i] as Interactable;
				addInteractable(c);
			}
		}
		public function removeLevel(lvl:Level):void {
			levelgroup.remove(lvl);
			levels[lvl.getIntName()] = null;
		}
		public function addInteractable(iable:Interactable):void {
			iables.push(iable);
			if(iable is FlxBasic) {
				everything.add(iable as FlxBasic);
				nonchars.add(iable as FlxBasic);
			}
		}
		public function removeInteractable(iable:Interactable):void {
			iables.splice(iables.indexOf(iable));
			if(iables is FlxBasic) {
				everything.remove(iable as FlxBasic);
				nonchars.remove(iable as FlxBasic);
			}
		}
		public function viewLevel(lvl:Level):Level {
			FlxG.camera.scroll.x = lvl.x;
			FlxG.camera.scroll.y = lvl.y;
			return lvl;
		}
		public function addCharacter(char:Character, lvl:Level):void {
			everything.add(char);
			chars.add(char);
			characters[char.id] = char;
			if(oldcharacters[char.id] == null) {
				oldcharacters[char.id] = new ActionClicks();
			}
			iables.push(char);
			char.lvl = lvl;
			char.x += lvl.x;
			char.y += lvl.y;
		}
		public function removeCharacter(char:Character):void {
			everything.remove(char);
			chars.remove(char);
			characters[char.id] = null;
			iables.splice(iables.indexOf(char));
		}
		public function moveCharacter(char:Character, lvl:Level):void {
			var charlvl:Level = char.lvl;
			if(charlvl != null) {
				char.x -= charlvl.x;
				char.y -= charlvl.y;
			}
			char.x += lvl.x;
			char.y += lvl.y;
			char.lvl = lvl;
		}
		public function resetAll():void {
			resetTimer();
			everything.clear();
			chars.clear();
			nonchars.clear();
			levelgroup.clear();
			front.clear();
			elevators = false;
			computers = true;
			while(iables.length > 0) iables.pop();
			for(var k:Object in characters) {
				characters[k] = null;
			}
			for(k in levels) {
				levels[k] = null;
			}
		}
		public function startNew():void {
			if(nextchar == 7) {
				FlxG.switchState(new EndState(this));
				return;
			}
			resetAll();
			addLevel(new ControlRoom(this, 0, 0));
			addLevel(new StorageRoom(200, 0));
			addLevel(new Hallway(this, 400, 0));
			addLevel(new ElevatorRoom(this, 600, 0));
			addLevel(new Office(this, 800, 0));
			addLevel(new ComputerRoom(this, 1000, 0));
			addLevel(new ExitRoom(this, 1200, 0));
			addLevel(new Outside(1400, 0));
			player = characters[nextchar++];
			for(var k:Object in oldcharacters) {
				(oldcharacters[k] as ActionClicks).start();
			}
		}
		public function resetTimer():void {
			if(nextsnd != null) {
				nextsnd.play(true);
			}
			timer = 0;
		}
		public function doclick(character:Character, p:FlxPoint, walkto:Boolean, talkto:Boolean):void {
			_rect.x = p.x; _rect.y = p.y; _rect.width = _rect.height = 0;
			if(talkto) interacttext.text = "Walk here";
			for(var i:Number = everything.length - 1; i >= 0; i--) {
				var o:FlxSprite = everything.members[i] as FlxSprite;
				if(o is Interactable &&  (o as Interactable).getScreenBox().overlaps(_rect)) {
					if(o == character) {
						if(talkto) interacttext.text = character.interactText(character);
						if(walkto) character.dosomething();
						walkto = false;
						break;
					} else if (iables.indexOf(o) >= 0) {
						if(talkto) {
							var ov:String = character.interactTextOverride(o as Interactable);
							if(ov == null) {
								interacttext.text = (o as Interactable).interactText(character);
							} else {
								interacttext.text = ov;
							}
						}
						if(walkto) {
							character.interacting = (o as Interactable);
							p = (o as Interactable).interactSpot(character);
						}
						break;
					}
				}
			}
			if(walkto) {
				if(character.shouldWalk(character.interacting)) {
					p.y = FlxU.max(p.y, character.lvl==null?80:character.lvl.y+80);
					character.walkTo(p);
				} else {
					character.walkTo(character.getFootPoint());
				}
			}
		}
		public function getCharacter(id:uint):Character {
			return characters[id] as Character;
		}
		override public function update():void {
			super.update();
			var ot:Number = timer;
			timer += FlxG.elapsed;
			FlxG.collide(chars, nonchars);
			everything.sort();
			cursor.x = FlxG.mouse.x; cursor.y = FlxG.mouse.y;
			viewLevel(player.lvl);
			if(FlxG.mouse.justReleased() && mutebutton.overlapsPoint(FlxG.mouse.getScreenPosition())) {
				if(FlxG.mute) {
					FlxG.mute = false;
					mutebutton.play("on");
				} else {
					FlxG.mute = true;
					mutebutton.play("off");
				}
			} else {
				doclick(player, FlxG.mouse.getWorldPosition(), FlxG.mouse.justReleased(), true);
				if(FlxG.mouse.justReleased()) {
					(oldcharacters[player.id] as ActionClicks).addPoint(timer, FlxG.mouse.getWorldPosition());
				}
			}
			for(var k:Object in oldcharacters) {
				var i:uint = k as uint;
				if (i == player.id) continue;
				if (oldcharacters[i] != null) {
					var cp:FlxPoint = (oldcharacters[i] as ActionClicks).nextPoint(timer);
					if(cp != null) {
						doclick(characters[i], cp, true, false);
					} else {
						
					}
				}
			}
			if(timer >= 10) {
				startNew();
			}
			if(FlxU.floor(timer) != FlxU.floor(ot)) {
				timertext.text = (10 - FlxU.floor(timer)).toString();
			}
		}
		public function win():void {
			won = true;
		}
		public function elevatorOn():void {
			elevators = true;
		}
		public function computerOff():void {
			computers = false;
		}
	}
}
