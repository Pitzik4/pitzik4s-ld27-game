package net.pitzik4.games.ld48.ld27 {
	import org.flixel.*;
	import net.pitzik4.games.ld48.ld27.char.Character;
	
	public interface Interactable {
		function interact(interacter:Character):void;
		function getScreenBox():FlxRect;
		function getInteractAnim(interacter:Character):String;
		function getInteractTime(interacter:Character):Number;
		function canInteract(interacter:Character):Boolean;
		function interactSpot(interacter:Character):FlxPoint;
		function interactText(interacter:Character):String;
		function facedir(interacter:Character):uint;
		function getpos():FlxPoint;
		function getCharName():String;
	}
}
