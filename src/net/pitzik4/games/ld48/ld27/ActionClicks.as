package net.pitzik4.games.ld48.ld27 {
	import org.flixel.*;
	
	public class ActionClicks {
		private var _times:Array;
		public var _points:Array;
		private var _pointer:uint;
		
		public function ActionClicks() {
			_times = new Array();
			_points = new Array();
			_pointer = 0;
		}
		
		public function start():void {
			_pointer = 0;
		}
		public function nextPoint(timer:Number):FlxPoint {
			if((_times[_pointer] as Number) <= timer) {
				return _points[_pointer++] as FlxPoint;
			}
			return null;
		}
		public function addPoint(timer:Number, point:FlxPoint):void {
			_times.push(timer);
			_points.push(point);
		}
	}
}
