package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class Desk extends FlxSprite implements Interactable {
		[Embed(source = "/gfx/bg/desk.png")] public static const ImgDesk:Class;
		[Embed(source = "/gfx/bg/lawsuit.png")] public static const ImgLawsuit:Class;
		
		public var open:Boolean;
		public var ps:PlayState;
		public var pov:FlxSprite;
		
		public function Desk(ps:PlayState, x:Number = 0, y:Number = 0) {
			super(x, y, ImgDesk);
			this.ps = ps;
			immovable = true;
			open = false;
			pov = new FlxSprite(0, 0, ImgLawsuit);
			pov.scrollFactor = _pZero;
		}
		
		public function interact(interacter:Character):void {
			if(interacter == null || interacter == ps.player) {
				open = !open;
				if(open) {
					ps.front.add(pov);
				} else {
					ps.front.remove(pov);
				}
			}
		}
		public function getScreenBox():FlxRect {
			if(open) {
				_rect.x = FlxG.camera.scroll.x; _rect.y = FlxG.camera.scroll.y; _rect.width = 160; _rect.height = 120;
			} else {
				_rect.x = x; _rect.y = y; _rect.width = 16; _rect.height = 32;
			}
			return _rect;
		}
		public function getInteractAnim(interacter:Character):String {
			return "idle";
		}
		public function getInteractTime(interacter:Character):Number {
			return 0;
		}
		public function canInteract(interacter:Character):Boolean {
			return true;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			if(open) return interacter.getFootPoint();
			var pos:FlxPoint = this.getpos();
			if(interacter.x > x) {
				pos.x += 16;
			} else {
				pos.x -= 16;
			}
			return pos;
		}
		public function interactText(interacter:Character):String {
			return "Read document";
		}
		public function facedir(interacter:Character):uint {
			if(open) return interacter.facing;
			if(interacter.x > x) {
				return LEFT;
			}
			return RIGHT;
		}
		public function getpos():FlxPoint {
			_point.x = x+8;
			_point.y = y+32;
			return _point;
		}
		public function getCharName():String {
			return "Desk";
		}
	}

}
