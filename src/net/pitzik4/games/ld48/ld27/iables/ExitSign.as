package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class ExitSign extends FlxSprite implements Interactable {
		public var ps:PlayState;
		public var gotolvl:String;
		public var odir:uint;
		public var newx:Number, newy:Number;
		
		public function ExitSign(ps:PlayState, gotolvl:String, x:Number = 0, y:Number = 0, newx:Number = 0, newy:Number = 0, odir:uint=12345) {
			super(x-1, y-1);
			makeGraphic(20, 8, 0);
			this.ps = ps;
			this.gotolvl = gotolvl;
			solid = false;
			this.newx = newx;
			this.newy = newy;
			this.odir = odir;
		}
		
		public function interact(interacter:Character):void {
			interacter.x = interacter.lvl.x+newx;
			interacter.y = interacter.lvl.y+newy;
			interacter.goToLevel(ps.levels[gotolvl]);
			if (odir != 12345) {
				interacter.facing = odir;
			}
		}
		public function getScreenBox():FlxRect {
			_rect.x = x; _rect.y = y; _rect.width = width; _rect.height = height;
			return _rect;
		}
		public function getInteractAnim(interacter:Character):String {
			return "walk";
		}
		public function getInteractTime(interacter:Character):Number {
			return 0.5;
		}
		public function canInteract(interacter:Character):Boolean {
			var fp:FlxPoint = interacter.getFootPoint();
			var mp:FlxPoint = getMidpoint();
			return true;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			var pos:FlxPoint = this.getMidpoint();
			pos.y += 32;
			return pos;
		}
		public function interactText(interacter:Character):String {
			return "Follow sign";
		}
		public function facedir(interacter:Character):uint {
			if(x < interacter.x) {
				return LEFT;
			}
			return RIGHT;
		}
		public function getpos():FlxPoint {
			return getMidpoint();
		}
		public function getCharName():String {
			return "Sign";
		}
	}
}
