package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class ControlPanel extends FlxSprite implements Interactable {
		[Embed(source = "/gfx/bg/controlpanel.png")] public static const ImgCP:Class;
		
		public var itext:String;
		public var onuse:Function;
		
		public function ControlPanel(itext:String, onuse:Function, x:Number = 0, y:Number = 0) {
			super(x, y, ImgCP);
			this.itext = itext;
			this.onuse = onuse;
			immovable = true;
		}
		
		public function interact(interacter:Character):void {
			onuse();
		}
		public function getScreenBox():FlxRect {
			_rect.x = x; _rect.y = y; _rect.width = width; _rect.height = height;
			return _rect;
		}
		public function getInteractAnim(interacter:Character):String {
			return "type";
		}
		public function getInteractTime(interacter:Character):Number {
			return 1;
		}
		public function canInteract(interacter:Character):Boolean {
			var fp:FlxPoint = interacter.getFootPoint();
			var mp:FlxPoint = getMidpoint();
			return FlxU.abs(fp.x - mp.x) <= 20 && FlxU.abs(fp.y - mp.y - height) <= 20;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			var pos:FlxPoint = this.getMidpoint();
			pos.y += 24;
			return pos;
		}
		public function interactText(interacter:Character):String {
			return itext;
		}
		public function facedir(interacter:Character):uint {
			return RIGHT;
		}
		public function getpos():FlxPoint {
			return getMidpoint();
		}
		public function getCharName():String {
			return "Control Panel";
		}
	}

}
