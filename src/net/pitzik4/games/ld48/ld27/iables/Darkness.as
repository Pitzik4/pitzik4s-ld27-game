package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class Darkness extends FlxSprite implements Interactable {
		public var open:Boolean;
		public var ps:PlayState;
		
		public static var it:Darkness = null;
		
		public function Darkness(ps:PlayState, x:Number = 0, y:Number = 0) {
			super(x, y);
			it = this;
			makeGraphic(160, 120, 0xFF000000);
			this.ps = ps;
			immovable = true;
			open = true;
			ps.front.add(this);
		}
		
		public function lightsOn():void {
			open = false;
			ps.front.remove(this);
			makeGraphic(8, 8, 0);
			width = height = 0;
		}
		
		public function interact(interacter:Character):void {
			
		}
		public function getScreenBox():FlxRect {
			if(open) {
				_rect.x = x; _rect.y = y; _rect.width = 160; _rect.height = 120;
			} else {
				_rect.x = x; _rect.y = y; _rect.width = 0; _rect.height = 0;
			}
			return _rect;
		}
		public function getInteractAnim(interacter:Character):String {
			return "idle";
		}
		public function getInteractTime(interacter:Character):Number {
			return 0;
		}
		public function canInteract(interacter:Character):Boolean {
			return true;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			return interacter.getFootPoint();
		}
		public function interactText(interacter:Character):String {
			return "The lights went out!";
		}
		public function facedir(interacter:Character):uint {
			return interacter.facing;
		}
		public function getpos():FlxPoint {
			_point.x = x;
			_point.y = y;
			return _point;
		}
		public function getCharName():String {
			return "Darkness";
		}
	}

}
