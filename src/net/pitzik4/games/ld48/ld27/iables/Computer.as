package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class Computer extends Fusebox {
		[Embed(source = "/gfx/bg/computer.png")] public static const ImgComputer:Class;
		
		public var ps:PlayState;
		
		public function Computer(ps:PlayState, onuse:Function, x:Number = 0, y:Number = 0, gfc:Class = null) {
			super(onuse, x, y, gfc == null?ImgComputer:gfc);
			this.ps = ps;
		}
		
		override public function canInteract(interacter:Character):Boolean {
			return ps.computers && interacter is Jen;
		}
		override public function interactSpot(interacter:Character):FlxPoint {
			if(!(interacter is Jen)) return interacter.getFootPoint();
			return super.interactSpot(interacter);
		}
		override public function interactText(interacter:Character):String {
			return ps.computers ? (interacter is Jen ? "Resolve issues" : "The computer is hacked.") : "The computer is off.";
		}
		override public function getCharName():String {
			return "Computer";
		}
	}

}
