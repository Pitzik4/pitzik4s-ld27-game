package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class PapersBox extends FlxSprite implements Interactable {
		[Embed(source = "/gfx/bg/newspaper.png")] public static const ImgNP:Class;
		
		public var open:Boolean;
		public var ps:PlayState;
		
		public function PapersBox(ps:PlayState, x:Number = 0, y:Number = 0) {
			super(x, y);
			this.ps = ps;
			loadGraphic(ImgNP, true, false, 160, 120);
			immovable = true;
			addAnimation("closed", [1]);
			addAnimation("open", [0]);
			open = true;
			interact(null);
			solid = false;
		}
		
		public function interact(interacter:Character):void {
			if(interacter == null || interacter == ps.player) {
				open = !open;
				if(open) {
					play("open");
					offset.x = offset.y = 0;
					x -= 48; y -= 88;
					ps.front.add(this);
				} else {
					play("closed");
					offset.x = 48;
					offset.y = 88;
					x += 48; y += 88;
					ps.front.remove(this);
				}
			}
		}
		public function getScreenBox():FlxRect {
			if(open) {
				_rect.x = x; _rect.y = y; _rect.width = width; _rect.height = height;
			} else {
				_rect.x = x; _rect.y = y-8; _rect.width = _rect.height = 16;
			}
			return _rect;
		}
		public function getInteractAnim(interacter:Character):String {
			return open ? "reading" : "takeread";
		}
		public function getInteractTime(interacter:Character):Number {
			return 2048;
		}
		public function canInteract(interacter:Character):Boolean {
			return true;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			if(open) return interacter.getFootPoint();
			var pos:FlxPoint = this.getpos();
			pos.y++;
			if(interacter.x > x) {
				pos.x += 8;
			} else {
				pos.x -= 8;
			}
			return pos;
		}
		public function interactText(interacter:Character):String {
			return "Read newspapers";
		}
		public function facedir(interacter:Character):uint {
			if(open) return interacter.facing;
			if(interacter.x > x) {
				return LEFT;
			}
			return RIGHT;
		}
		public function getpos():FlxPoint {
			_point.x = x+8;
			_point.y = y+8;
			return _point;
		}
		public function getCharName():String {
			return "Box of Newspapers";
		}
	}

}
