package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class Door extends FlxSprite implements Interactable {
		[Embed(source = "/gfx/bg/door.png")] public static const ImgDoor:Class;
		
		public var ps:PlayState;
		public var gotolvl:String;
		public var dir:uint, odir:uint;
		public var offx:Number, offy:Number;
		
		public function Door(ps:PlayState, gotolvl:String, x:Number = 0, y:Number = 0, offx:Number = 0, offy:Number = 0, odir:uint=0, gfc:Class = null, dir:uint = DOWN) {
			super(x, y, gfc == null?ImgDoor:gfc);
			this.ps = ps;
			this.gotolvl = gotolvl;
			solid = false;
			this.offx = offx;
			this.offy = offy;
		}
		
		public function interact(interacter:Character):void {
			interacter.x += offx;
			interacter.y += offy;
			interacter.goToLevel(ps.levels[gotolvl]);
			if(odir != 0) {
				interacter.facing = odir;
			}
		}
		public function getScreenBox():FlxRect {
			_rect.x = x-offset.x; _rect.y = y-offset.y; _rect.width = width; _rect.height = height;
			return _rect;
		}
		public function getInteractAnim(interacter:Character):String {
			return "walk";
		}
		public function getInteractTime(interacter:Character):Number {
			return 0.5;
		}
		public function canInteract(interacter:Character):Boolean {
			var fp:FlxPoint = interacter.getFootPoint();
			var mp:FlxPoint = getMidpoint();
			return FlxU.abs(fp.x - mp.x) <= 20 && FlxU.abs(fp.y - mp.y - 12 + offset.y) <= 20;
		}
		public function interactSpot(interacter:Character):FlxPoint {
			var pos:FlxPoint = this.getMidpoint();
			pos.y += 16;
			pos.y -= offset.y;
			if(dir == LEFT) {
				pos.x -= 16;
			} else if(dir == RIGHT) {
				pos.x += 16;
			} else if(dir == DOWN) {
				pos.y += 16
			} else {
				pos.y -= 16;
			}
			return pos;
		}
		public function interactText(interacter:Character):String {
			return "Enter door";
		}
		public function facedir(interacter:Character):uint {
			if(x < interacter.x) {
				return LEFT;
			}
			return RIGHT;
		}
		public function getpos():FlxPoint {
			return getMidpoint();
		}
		public function getCharName():String {
			return "Door";
		}
	}
}
