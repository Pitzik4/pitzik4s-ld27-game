package net.pitzik4.games.ld48.ld27.iables {
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.levels.*;
	import org.flixel.*;
	
	public class Elevator extends Door {
		[Embed(source = "/gfx/bg/elevator.png")] public static const ImgElevator:Class;
		
		public var broken:Boolean;
		
		public function Elevator(ps:PlayState, gotolvl:String, x:Number = 0, y:Number = 0, offx:Number = 0, offy:Number = 0, odir:uint=0, gfc:Class = null, dir:uint = DOWN) {
			super(ps, gotolvl, x, y, offx, offy, odir, gfc == null?ImgElevator:gfc, dir);
			broken = true;
		}
		
		override public function interact(interacter:Character):void {
			if(!broken && ps.elevators) {
				super.interact(interacter);
			} else if(interacter is Mike) {
				interacter.capable = false;
			}
		}
		override public function getInteractAnim(interacter:Character):String {
			if(broken) {
				if(interacter is Mike) {
					return "crowbar";
				} else {
					return "idle";
				}
			} else {
				if(ps.elevators) {
					return "walk";
				} else {
					return "idle";
				}
			}
		}
		override public function getInteractTime(interacter:Character):Number {
			if(broken) {
				if(interacter is Mike) {
					return 7.5;
				} else {
					return 0;
				}
			} else {
				if(ps.elevators) {
					return 0.5;
				} else {
					return 0;
				}
			}
		}
		override public function interactText(interacter:Character):String {
			if(broken) {
				if(interacter is Mike) {
					return "Pry open elevator";
				} else {
					return "The elevator is broken.";
				}
			} else {
				if(ps.elevators) {
					return "Use elevator";
				} else {
					return "The elevator is off.";
				}
			}
		}
		override public function interactSpot(interacter:Character):FlxPoint {
			var pos:FlxPoint = super.interactSpot(interacter);
			if(interacter is Mike) {
				if(interacter.x < x) {
					pos.x -= 16;
				} else {
					pos.x += 16
				}
			}
			return pos;
		}
		override public function getCharName():String {
			return broken?"Broken Elevator":"Elevator";
		}
	}
}
