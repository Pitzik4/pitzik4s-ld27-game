package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class Outside extends Level {
		[Embed(source = "/gfx/bg/outside.png")] public static const OutsideBG:Class;
		
		public function Outside(x:Number = 0, y:Number = 0) {
			super("outside", x, y, OutsideBG);
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [];
		}
	}
}
