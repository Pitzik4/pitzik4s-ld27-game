package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class StorageRoom extends Level {
		[Embed(source = "/gfx/bg/storage.png")] public static const SRoomBG:Class;
		
		public function StorageRoom(x:Number = 0, y:Number = 0) {
			super("storage", x, y, SRoomBG);
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [new Kate(ps, 50, 100, 1)];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [new Door(ps, "hallway", x+12, y+48, 128, 0, LEFT), new PapersBox(ps, x, y)];
		}
	}
}
