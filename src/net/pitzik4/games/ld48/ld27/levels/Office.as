package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class Office extends Level {
		[Embed(source = "/gfx/bg/office.png")] public static const OfficeBG:Class;
		
		public var ps:PlayState;
		public var darkness:Darkness;
		
		public function Office(ps:PlayState, x:Number = 0, y:Number = 0) {
			super("office", x, y, OfficeBG);
			darkness = new Darkness(ps, x, y);
			this.ps = ps;
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [new Jen(ps, 32, 102, 3)];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [new Door(ps, "elevator", x+16, y+48, 124, 0, LEFT), new Desk(ps, x+120, y+70), darkness];
		}
	}
}
