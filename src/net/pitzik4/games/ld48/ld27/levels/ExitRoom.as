package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class ExitRoom extends Level {
		[Embed(source = "/gfx/bg/exitroom.png")] public static const ExitRoomBG:Class;
		
		public var ps:PlayState;
		public var elevator:Elevator;
		
		public function ExitRoom(ps:PlayState, x:Number = 0, y:Number = 0) {
			super("exit", x, y, ExitRoomBG);
			elevator = new Elevator(ps, "elevator", x + 88, y + 48);
			elevator.broken = false;
			this.ps = ps;
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [new Tyler(ps, 16, 100, 6)];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [elevator, new ExitSign(ps, "hallway", x+135, y+63, 16, 100, RIGHT), new ExitSign(ps, "outside", x+13, y+63, 50, 56, LEFT)];
		}
	}
}
