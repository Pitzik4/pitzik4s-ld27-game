package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class ComputerRoom extends Level {
		[Embed(source = "/gfx/bg/comproom.png")] public static const CompBG:Class;
		
		public var ps:PlayState;
		
		public function ComputerRoom(ps:PlayState, x:Number = 0, y:Number = 0) {
			super("computer", x, y, CompBG);
			this.ps = ps;
		}
		
		public function unhack():void {
			ps.win();
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [new Goon(ps, 16, 100, 4), new MrChase(ps, 48, 100, 5)];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [new Computer(ps, unhack, x+100, y+60), new Door(ps, "hallway", x+128, y+48, -112, 0, RIGHT)];
		}
	}
}
