package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class ElevatorRoom extends Level {
		[Embed(source = "/gfx/bg/eleroom.png")] public static const EleBG:Class;
		
		public var ps:PlayState;
		public var elevator:Elevator;
		
		public function ElevatorRoom(ps:PlayState, x:Number = 0, y:Number = 0) {
			super("elevator", x, y, EleBG);
			elevator = new Elevator(ps, "exit", x + 88, y + 48);
			this.ps = ps;
		}
		
		public function fixSwitch():void {
			elevator.broken = false;
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [new Mike(ps, 16, 100, 2)];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [elevator, new ElevatorSwitch(fixSwitch, x + 124, y + 60)];
		}
	}
}
