package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class Level extends FlxSprite {
		[Embed(source = "/gfx/bg/test.png")] public static const TestBG:Class;
		
		protected var _name:String;
		
		public function Level(name:String, x:Number = 0, y:Number = 0, img:Class = null) {
			super(x, y, img == null?TestBG:img);
			this._name = name;
		}
		
		public function getCharacters(ps:PlayState):Array {
			return [];
		}
		public function getInteractables(ps:PlayState):Array {
			return [];
		}
		public function getIntName():String {
			return _name;
		}
	}
}
