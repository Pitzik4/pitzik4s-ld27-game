package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class Hallway extends Level {
		[Embed(source = "/gfx/bg/hallway.png")] public static const HallBG:Class;
		
		public var ps:PlayState;
		public var door:Door;
		
		public function Hallway(ps:PlayState, x:Number = 0, y:Number = 0) {
			super("hallway", x, y, HallBG);
			door = new Door(ps, "computer", x + 16, y + 120, 112, 0, LEFT, null, UP);
			door.offset.y = 32;
			this.ps = ps;
		}
		
		public function switchFuse():void {
			Darkness.it.lightsOn();
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [new Fusebox(switchFuse, x+100, y+60), new ExitSign(ps, "exit", x+13, y+63, 128, 100, LEFT), door];
		}
	}
}
