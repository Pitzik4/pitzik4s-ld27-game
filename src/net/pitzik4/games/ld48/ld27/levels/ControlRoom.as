package net.pitzik4.games.ld48.ld27.levels {
	import net.pitzik4.games.ld48.ld27.char.*;
	import net.pitzik4.games.ld48.ld27.*;
	import net.pitzik4.games.ld48.ld27.iables.*;
	import org.flixel.*;
	
	public class ControlRoom extends Level {
		[Embed(source = "/gfx/bg/controlroom.png")] public static const CRoomBG:Class;
		
		public var ps:PlayState;
		
		public function ControlRoom(ps:PlayState, x:Number = 0, y:Number = 0) {
			super("control", x, y, CRoomBG);
			this.ps = ps;
		}
		public function teleportKate():void {
			ps.moveCharacter(ps.characters[1] as Character, this);
		}
		
		override public function getCharacters(ps:PlayState):Array {
			return [new Jake(ps, 56, 100, 0)];
		}
		override public function getInteractables(ps:PlayState):Array {
			return [new ControlPanel("Turn on elevators", ps.elevatorOn, 40, 64), new ControlPanel("Turn off computers", ps.computerOff, 104, 64)];
		}
	}
}
