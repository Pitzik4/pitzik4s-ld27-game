package net.pitzik4.games.ld48.ld27 {
	import net.pitzik4.games.ld48.ld27.char.Character;
	import net.pitzik4.games.ld48.ld27.char.Goon;
	import net.pitzik4.games.ld48.ld27.levels.Office;
	import org.flixel.*;
 
	public class EndState extends FlxState {
		public var title:FlxText;
		public var instructions:FlxText;
		public var ps:PlayState;
		public var texts:Array;
		public var done:Boolean = false;
		public var timer:Number = 0;
		
		public function EndState(ps:PlayState) {
			super();
			this.ps = ps;
		}
		
		override public function create():void {
			FlxG.bgColor = 0xFF000000;
			FlxG.playMusic(TitleState.MusTitle);
			TitleState.titlemusic = true;
			
			var ypoint:Number = 4;
			texts = new Array();
			
			title = new FlxText(4, ypoint, 160, ps.won ? "Jackton Factory is safe." : "Jackton Factory is gone.");
			title.alpha = 0;
			texts.push(title);
			add(title);
			ypoint += 12;
			
			var alive:Array = new Array();
			for (var i:uint = 0; i < 7; i++) {
				if ((ps.characters[i] as Character).capable && (ps.won ? true : (ps.characters[i] as Character).lvl.getIntName() == "outside")) {
					alive.push(ps.characters[i]);
				}
			}
			if(alive.length == 0) {
				title = new FlxText(4, ypoint, 160, "There were no survivors.");
				title.alpha = 0;
				texts.push(title);
				add(title);
				ypoint += 12;
			} else if(alive.length == 7) {
				title = new FlxText(4, ypoint, 160, "There were no casualties.");
				title.alpha = 0;
				texts.push(title);
				add(title);
				ypoint += 12;
			} else {
				title = new FlxText(4, ypoint, 160, alive.length.toString() + " of 7 people survived.");
				title.alpha = 0;
				texts.push(title);
				add(title);
				ypoint += 12;
			}
			if((ps.characters[4] as Goon).hostage && alive.indexOf(ps.characters[4]) >= 0) {
				title = new FlxText(4, ypoint, 160, "The hostage was shot after.");
				title.alpha = 0;
				texts.push(title);
				add(title);
				ypoint += 12;
			}
			var fleers:Array = new Array();
			if(alive.indexOf(ps.characters[6]) >= 0 && alive.indexOf(ps.characters[4]) >= 0 && alive.indexOf(ps.characters[5]) >= 0) fleers.push("Tyler");
			if(alive.indexOf(ps.characters[1]) >= 0 && (ps.levels["office"] as Office).darkness.open) fleers.push("Kate");
			if(alive.indexOf(ps.characters[3]) >= 0 && !ps.won) fleers.push("Jen");
			if(fleers.length > 0) {
				var str:String;
				if(fleers.length == 1) {
					str = fleers[0];
				} else if(fleers.length == 2) {
					str = fleers[0] + " and " + fleers[1];
				} else {
					trace(fleers.length);
					str = "Tyler, Kate, and Jen";
				}
				title = new FlxText(4, ypoint, 160, str + " ran away and doomed them all.");
				title.alpha = 0;
				texts.push(title);
				add(title);
				ypoint += title.height + 4;
			}
			
			instructions = new FlxText(4, 108, 152, "Any key skips.");
			add(instructions);
		}
		public function skip():void {
			for(var i:uint = 0; i < texts.length; i++) {
				var c:FlxText = texts[i] as FlxText;
				if(c != null) {
					(c as FlxText).alpha = 1;
				}
			}
			complete();
		}
		public function complete():void {
			done = true;
			instructions.text = "Z: Title. X: Retry.";
		}
		override public function update():void {
			super.update();
			timer += FlxG.elapsed;
			if(done) {
				if(FlxG.keys.Z) {
					FlxG.switchState(new TitleState());
				} else if(FlxG.keys.X) {
					FlxG.switchState(new PlayState());
				}
			} else {
				var t:Number = FlxU.floor(timer / 3);
				if(t < texts.length) {
					(texts[t] as FlxText).alpha = FlxU.max(0, (timer - t*3)*0.6667-1);
				}
				if(t >= texts.length) {
					complete();
				}
				if (FlxG.keys.any()) {
					skip();
				}
			}
		}
	}
}
