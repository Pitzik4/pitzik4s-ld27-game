package net.pitzik4.games.ld48.ld27 {
	import org.flixel.*;
 
	public class TitleState extends FlxState {
		[Embed(source = "/mus/castle-of-a-bear.mp3")] public static const MusTitle:Class;
		
		public var title:FlxText;
		public var instructions:FlxText;
		public static var titlemusic:Boolean = false;
		
		override public function create():void {
			FlxG.bgColor = 0xFF6E6E6E;
			if(!titlemusic) {
				FlxG.playMusic(MusTitle);
				titlemusic = true;
			}
			
			title = new FlxText(35, 32, 90, "Jackton Factory");
			add(title);
			
			instructions = new FlxText(4, 108, 152, "Z: Start. X: Help. C: About.");
			add(instructions);
		}
		override public function update():void {
			super.update();
			if(FlxG.keys.Z) {
				FlxG.switchState(new PlayState());
			} else if(FlxG.keys.X) {
				FlxG.switchState(new HelpState());
			} else if(FlxG.keys.C) {
				FlxG.switchState(new AboutState());
			}
		}
	}
}
