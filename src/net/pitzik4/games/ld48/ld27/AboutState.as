package net.pitzik4.games.ld48.ld27 {
	import org.flixel.*;
 
	public class AboutState extends FlxState {
		public var title:FlxText;
		public var instructions:FlxText;
		
		override public function create():void {
			FlxG.bgColor = 0xFF6E6E6E;
			
			title = new FlxText(4, 4, 152, "Jackton Factory was created in 57 hours by Pitzik4 for the 27th triannual Ludum Dare Game Jam.");
			add(title);
			
			instructions = new FlxText(4, 108, 152, "Z: Return to title.");
			add(instructions);
		}
		override public function update():void {
			super.update();
			if(FlxG.keys.Z) {
				FlxG.switchState(new TitleState());
			}
		}
	}
}
